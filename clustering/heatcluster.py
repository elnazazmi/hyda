
__author__ = 'Elnaz Azmi'
__email__ = 'elnaz.azmi@kit.edu'
__status__ = 'Development'

import numpy as np
import heat as ht


# apply K-medoids
def heat_kmedoids(data, k):
    htmedoids = ht.cluster.KMedoids(k)
    htdata = ht.array(data.to_numpy())
    htmedoids.fit(htdata)
    pred = htmedoids.cluster_centers_

    represent = np.array([], dtype=np.int)

    for i in range(pred.shape[0]):
        represent = np.append(represent, data[data.isin(pred[i].numpy()).all(axis=1)].index.values[0])

    return [pred, represent]
